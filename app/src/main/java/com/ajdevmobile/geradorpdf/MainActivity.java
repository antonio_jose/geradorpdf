package com.ajdevmobile.geradorpdf;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.pdf.PdfDocument;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;

public class MainActivity extends AppCompatActivity {

    private EditText edit_conteudo;
    private Button bt_gerar_pdf;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edit_conteudo = findViewById(R.id.edit_conteudo);
        bt_gerar_pdf = findViewById(R.id.button_gerar_pdf);

        bt_gerar_pdf.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if(edit_conteudo.getText().toString().isEmpty()){

                        exibirMensagem( "Campo vázio . . . " );

                }else{
                    String txt = edit_conteudo.getText().toString();
                    gerar_pdf(txt);
                }

            }
        });


    }


    public void gerar_pdf(String conteudo){

        PdfDocument pdfDocument = new PdfDocument();

        PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder(600, 900, 1).create();

        PdfDocument.Page page1 = pdfDocument.startPage( pageInfo );

        Canvas canvas = page1.getCanvas();

        Paint corTexto = new Paint();
        corTexto.setColor(Color.BLACK);

        Paint corTexto1 = new Paint();
        corTexto1.setColor(Color.BLUE);

        Paint corTexto2 = new Paint();
        corTexto2.setColor(Color.RED);

        String texto = "================ Comprovante ===================";

        canvas.drawText(texto, 80, 20, corTexto);
        canvas.drawText(conteudo, 10, 40, corTexto);
        canvas.drawText(conteudo, 10, 60, corTexto1);
        canvas.drawText(conteudo, 10, 80, corTexto2);

        pdfDocument.finishPage(page1);

        //================================================================ //

        //String path = "/sdcard/pdf/exemplo.pdf";

        File filePath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        String path = filePath+"/exemplo.pdf";
        File file = new File( path );

        try{

            pdfDocument.writeTo(new FileOutputStream(file));
            exibirMensagem( "Pdf gerado com sucesso . . . " );


        }catch (Exception e){
            exibirMensagem( "Erro ao criar pdf . . . "+e.getMessage());
        }

        pdfDocument.close();

    }


    private void exibirMensagem(String mensagem){

        Toast.makeText(this, mensagem, Toast.LENGTH_LONG).show();
    }
}